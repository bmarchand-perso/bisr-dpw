from rna_interface import random_conflict_graph, extend_to_pm
from maximum_matching import maximum_matching
from dpw_interface import construct_digraph
from barriers_py3 import realize, ssrandom, ssparse, sstopairs, ssstring
from dpw_algorithm import DPW
from graph_classes import Digraph
import random

import matplotlib.pylab as plt

seed = 2021

def test_agreement_dpw_mcf_small_graphs():

    ns = [10, 15, 20]

    nruns = 10

    for n in ns:
        for inc in range(nruns):
            print("n inc ",n, inc)
            k1, n_ur = solve_dpw(n, seed+inc)
            print("n_ur", n_ur)
            k2 = solve_mcf(n, seed+inc)

            assert(k1+1==k2+n_ur)

def test_small_instance1():

    n = 20

    k1, n_ur = solve_dpw(n, seed)
    k2 = solve_mcf(n, seed)

    assert(k1+1==k2+n_ur)

def test_small_instance2():

    n = 20
    
    k1, n_ur = solve_dpw(n, seed + 1)
    k2 = solve_mcf(n, seed + 1)

    assert(k1+1==k2+n_ur)

def test_small_instance3():

    n = 20

    k1, n_ur = solve_dpw(n, seed + 62)
    k2 = solve_mcf(n, seed + 62)

    assert(k1+1==k2+n_ur)

def test_small_instance4():

    n = 10

    k1, n_ur = solve_dpw(n, seed)
    k2 = solve_mcf(n, seed)

    assert(k1+1==k2+n_ur)

def solve_dpw(n, seed):

    G = random_conflict_graph(n, seed=seed, theta=1, unpaired_weight=0.2)

    if len(G.left)==0:
        return -1, 0

    M = maximum_matching(G)
    
    M_dict = {}

    for u,v in M:
        M_dict[u] = v
        M_dict[v] = u

    n_ur = 0
    for u in G.right:
        if u not in M_dict.keys():
            n_ur += 1 
    
    G, M = extend_to_pm(G,M)
    
    M_dict = {}

    for u,v in M:
        M_dict[u] = v
        M_dict[v] = u


    H, int_to_e, e_to_int = construct_digraph(G.left, G.right, G, M_dict)     
 
    k = 0

    while k <= n:
    
        seq = DPW(H, k, break_into_scc=True)      
 
        if seq is not None:
            break

        k+= 1
    
    return k, n_ur 

def solve_mcf(n, seed):

    theta = 1

    random.seed(seed)
    count = {}
    
    s1 = ssrandom(n, count, theta=theta, unpaired_weight=0.2)
    s2 = ssrandom(n, count, theta=theta, unpaired_weight=0.2)

    if s2.count("(") > s1.count("("):
        s1, s2 = s2, s1

    ss1 = ssparse(s1)
    ss2 = ssparse(s2)
    bps1 = sstopairs(ss1)
    bps2 = sstopairs(ss2)

    # Remove common base pairs
    commonbps = bps1 & bps2
    if len(commonbps) > 0:
        for (i, j) in commonbps:
            (ss1[i], ss1[j]) = (-1, -1)
            (ss2[i], ss2[j]) = (-1, -1)
        s1 = ssstring(ss1)
        s2 = ssstring(ss2)
        bps1 = sstopairs(ss1)
        bps2 = sstopairs(ss2)

    k = 0

    while True and k < n:
        
        p = realize(ss1, ss2, k, 1, theta=theta)
        if p is not None:
            break
        k += 1


    return k

if __name__=='__main__':
    test_agreement_dpw_mcf_small_graphs()
