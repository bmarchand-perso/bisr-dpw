from graph_classes import Digraph
from barriers_py3 import ssparse, sstopairs
import networkx as nx

def secondary_structures_to_G(s1, s2):

    bps1 = sstopairs(s1)
    bps2 = sstopairs(s2)

    # Remove common base pairs
    commonbps = bps1 & bps2
    if len(commonbps) > 0:
        for (i, j) in commonbps:
            (s1[i], s1[j]) = (-1, -1)
            (s2[i], s2[j]) = (-1, -1)
        bps1 = sstopairs(s1)
        bps2 = sstopairs(s2)

    G = nx.Graph()
    B = set([])
    R = set([])

    for u in bps1:
        G.add_node(u)
        B.add(u)
        for v in bps2:
            G.add_node(v)
            R.add(v)

            conflict = False

            if len(set([u[0], v[0], u[1], v[1]])) != 4:
                conflict = True

            if (u[0] < v[0] < u[1] < v[1]) or (v[0] < u[0] < v[1] < u[1]):
                conflict = True

            if conflict:
                G.add_edge(u, v)

    return B,R,G

def construct_digraph(B,R,G,M):


    H = Digraph()

    e_to_int = {}
    int_to_e = {}

    for l, u in enumerate(B):
        e_to_int[(u, M[u])] = l
        int_to_e[l] = (u, M[u])
        H.add_node(l)

    
    for u in B:
        for v in B:
            if u != v:
                if G.has_edge(u, M[v]):
                    H.add_edge(e_to_int[(u,M[u])], e_to_int[(v,M[v])])

    return H, int_to_e, e_to_int

def extend_to_pm_nx(B, R, G, M):

    N = G.order()

    for b in B:
        if b not in M.keys():
            G.add_node(N)
            R.add(N) 
            for b2 in B:
                G.add_edge(b2, N)
            M[N] = b
            M[b] = N

            N += 1

    for r in R:
        if r not in M.keys():
            G.add_node(N)
            B.add(N)
            for r2 in R:
                G.add_edge(r2, N)
            M[N] = r
            M[r] = N

            N += 1

    return B, R, G, M
