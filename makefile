SO_LIB_NAME = python/cpp_routines`python3-config --extension-suffix`
FLAGS = -O3 -Wall -shared -std=c++11 -fPIC `python3 -m pybind11 --includes`
OSX_FLAGS = -O3 -Wall -shared -std=c++11 -undefined dynamic_lookup -fPIC `python3 -m pybind11 --includes`
SRC_FILE = cpp/cpp_routines.cpp

linux:
	c++ $(FLAGS) $(SRC_FILE) -o $(SO_LIB_NAME)

macos:
	c++ $(OSX_FLAGS) $(SRC_FILE) -o $(SO_LIB_NAME)

check:
	python3 -m pytest test/

clean:
	rm lib/*.so
